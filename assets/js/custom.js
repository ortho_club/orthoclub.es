// PIXEL DE FACEBOOK

// Botón Menú Contacto
document.querySelector('.menu__btn-contact').addEventListener('click', () => {
  fbq('trackCustom', 'Menu_contacto')
})

// Botones Submenú Servicios/Formación
document.querySelectorAll('.menu__link[href="#formacion"] + .menu__subitems .menu__link').forEach(e => {
  e.addEventListener('click', c => {
    const id = c.target.hash.replace('#', '')
    fbq('trackCustom', `Menu_${id}`)
  })
})

// Botones Servicios/Formación
document.querySelectorAll('#formacion .btn').forEach(e => {
  e.addEventListener('click', c => {
    const id = c.target.hash.replace('#', '')
    fbq('trackCustom', `Mas_${id}`)
  })
})

// Botones Servicios/Formación Modal Subservicios
document.querySelectorAll('#formacion ~ .modal .grid__item .btn[href="#contacto"]').forEach(e => {
  e.addEventListener('click', btn => {
    const id = location.hash.replace('#', '')
    const title = btn.target.closest('.grid__item').querySelector('.section__title').innerText.replace(' ', '-')
    fbq('trackCustom', `ReservaTuPlaza_${id}_${title}`)
  })
})

// Botón Servicios/Formación Modal Inscríbete
document.querySelector('#planificacion-clinchecks .btn[href="#contacto"]').addEventListener('click', () => {
  fbq('trackCustom', 'Inscribete_planificacion-clinchecks')
})
