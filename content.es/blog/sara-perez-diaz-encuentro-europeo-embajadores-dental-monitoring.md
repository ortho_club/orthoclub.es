---
title: "La Dr. Sara Pérez Díaz acude al Encuentro Europeo de Embajadores de Dental Monitoring"
title_seo: ""
slug: sara-perez-diaz-encuentro-europeo-embajadores-dental-monitoring
description: "Sara Pérez Díaz, ortodoncista de Vélez & Lozano, asistió el pasado 20 de abril al Encuentro Europeo de Embajadores de Dental Monitoring."
image: "sara-perez-diaz-encuentro-europeo-embajadores-dental-monitoring.jpg"
categories: []
tags: []
author: admin
toc: false
draft: false
noindex: true
date: 2020-04-26
lastmod: 2020-04-26
---                                                                                 
Sara Pérez Díaz, ortodoncista de [Vélez & Lozano](https://velezylozano.com/), asistió el pasado 20 de abril al **Encuentro Europeo de Embajadores de Dental Monitoring**.

<!-- more -->

Dental Monitoring es pionera en seguimientos virtuales en odontología mediante inteligencia artificial. A través de la aplicación móvil, los pacientes pueden **“monitorizar” su tratamiento de ortodoncia desde casa.** Esta aplicación pedirá al paciente una serie de registros que serán analizados por la inteligencia artificial, y proporcionarán datos clínicos a los profesionales para indicar posteriormente a los pacientes cuáles son los siguientes pasos.

La innovación aplicada a la **Odontología** ha revolucionado por completo todo el sector: los pacientes pueden llevar a cabo un tratamiento de ortodoncia de forma semipresencial, de manera que, desde casa, mantienen un contacto estrecho con el ortodoncista, que le dará toda la información e indicaciones del tratamiento.

> Es muy cómodo para los pacientes, ya que en menos de 1 minuto se escanean, y momentos después pueden ver reflejado en su teléfono móvil la evolución de su tratamiento y las indicaciones que recomendamos seguir desde la clínica. Sin duda, en estos tiempos de pandemia, la reducción de desplazamientos es un factor importante para que los pacientes se sientan tranquilos