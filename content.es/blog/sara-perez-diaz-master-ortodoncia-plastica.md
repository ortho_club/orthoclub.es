---
title: La Dra. Sara Pérez Díaz asiste al módulo del Máster intensivo de
  Ortodoncia Plástica del Dr. Arturo Vela
title_seo: Dra. Sara Pérez, conferenciante del congreso SEDA 2020
slug: sara-perez-diaz-master-ortodoncia-plastica
description: La Dra. Sara Pérez Díaz, acudió el pasado 16 de octubre al último
  módulo del Máster intensivo de Ortodoncia Plástica, impartido por...
categories: []
tags: []
author: admin
image: sara-perez-diaz-master-ortodoncia-plastica.jpg
toc: false
draft: false
noindex: true
date: 2020-10-21
lastmod: 2020-10-21
---
La **Dra. Sara Pérez Díaz**, ortodoncista y tutora de OrthoClub ACADEMY acudió el pasado 16 de octubre al último módulo del **Máster intensivo de Ortodoncia Plástica**, impartido por el Dr. Arturo Vela en Madrid.

<!-- more -->

**Sara Pérez está en formación constante.** Su práctica clínica no sólo implica tratar a los pacientes, sino estar plenamente actualizados para que estos tratamientos lleguen correctamente a término. Por eso la Doctora quiso apostar por el máster formativo de ortodoncia plástica que imparten **Arturo Vela y Nacho Morales** en Madrid. En este curso modular se trataron diversas áreas del manejo de casos de ortodoncia con alineadores, desde las bases más teóricas hasta el tratamiento de los casos más complejos. Arturo Vela y Nacho Morales explicaron a la perfección la **biomecánica aplicada al plástico**, abordando también el manejo de casos multidisciplinares. El Dr. Arturo Vela-Hernández es Doctor en Medicina, ortodoncista de práctica exclusiva y profesor del máster de Ortodoncia de la Universidad de Valencia. Autor de numerosas publicaciones y profesor de diversos cursos, cuenta con más de 2500 pacientes tratados con ortodoncia plástica.

> Es un máster avanzado, de la mano de dos ortodoncistas de muy alto nivel, reconocidos a nivel mundial. No explican sólo los casos en los que han tenido éxito, sino también cómo resolver los errores que pueden ocurrir durante el proceso. Sin duda alguna, es un máster de obligada asistencia.