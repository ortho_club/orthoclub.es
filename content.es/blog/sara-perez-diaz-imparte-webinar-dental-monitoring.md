---
title: "Sara Pérez Díaz imparte el webinar “Dental Monitoring: de la teoría a la
  práctica”"
title_seo: Sara Pérez Díaz imparte el webinar Dental Monitoring
slug: sara-perez-diaz-imparte-webinar-dental-monitoring
description: "La Dra. Sara Pérez Díaz, ortodoncista, impartió el pasado 22 de
  abril el webinar “Dental Monitoring: de la teoría a la práctica”..."
categories: []
tags: []
author: admin
image: sara-perez-diaz-imparte-webinar-dental-monitoring.jpg
toc: false
draft: false
noindex: true
date: 2021-04-29
lastmod: 2021-04-29
---

La Dra. Sara Pérez Díaz, ortodoncista de práctica exclusiva impartió el pasado 22 de abril el webinar **“Dental Monitoring: de la teoría a la práctica”.**

<!-- more -->

El pasado 22 de Abril Sara Pérez impartió el webinar titulado “Dental Monitoring: de la teoría a la práctica”. Después de su intervención como ponente oficial en Dental Monitoring del día 14 de Noviembre, donde explicó las bases del sistema de **seguimiento virtual**, quiso realizar una nueva ponencia para explicar cómo se integra este sistema en los protocolos clínicos habituales. Además, la doctora hizo hincapié en las **numerosas ventajas** que tiene la aplicación de la inteligencia artificial en la consulta, tanto para el paciente como para el funcionamiento general de la clínica. Actualmente son más de 800.000 personas las que aplican la inteligencia artificial a sus tratamientos.

> Es un gran **cambio en el paradigma** establecido actualmente. Antes los pacientes acudían cada mes a revisión, y ahora, cada semana el paciente recibe información sobre la evolución de su tratamiento, y nosotros obtenemos una imagen diagnóstica sobre la que tomamos las decisiones clínicas oportunas. Los pacientes agradecen venir menos a consulta, y nosotros agradecemos en estos tiempos poder tener más tiempo para atender nuevos pacientes.