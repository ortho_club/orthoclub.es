---
title: Dra. Sara Pérez, conferenciante del congreso SEDA 2020
title_seo: Dra. Sara Pérez, conferenciante del congreso SEDA 2020
slug: dra-sara-perez-conferenciante-congreso-seda
description: La Dra Sara Pérez, especialista en ortodoncia invisible, asistió
  como ponente al congreso de la Sociedad Española De Alineadores...
image: dra-sara-perez-conferenciante-congreso-seda-2020.jpg
categories: []
tags: []
author: admin
toc: false
draft: false
noindex: false
date: 2020-10-01
lastmod: 2020-10-01
---
La Dra Sara Pérez, especialista en ortodoncia invisible, ponente en congreso de la Sociedad Española De Alineadores 2020.

<!-- more -->

La **Sociedad Española De Alineadores** celebra cada año un congreso multitudinario al que acuden ortodoncistas de todo el mundo con el objetivo de mejorar y actualizarse profesionalmente.\
En este congreso múltiples **ortodoncistas de prestigio** abordan ampliamente temas relacionados con los tratamientos de ortodoncia con alineadores, El pasado 11 de Septiembre la Dra Sara Pérez asistió a este congreso como conferenciante, abordando el tema de la inteligencia artificial aplicada a los tratamientos de ortodoncia, en una ponencia titulada **“Future is clear: Seguimiento virtual”.** En esta charla la Doctora quiso acercar a los ortodoncistas su experiencia en la aplicación de Dental Monitoring en sus pacientes. 

> Dental Monitoring es el reflejo del futuro hecho presente. En poco tiempo tras la aparición de la ortodoncia plástica disponemos de un sistema en el que podemos controlar de cerca la evolución del tratamiento, y además mantener al paciente informado sobre la evolución. 

Más de **800.000 pacientes** se escanean a día de hoy con este sistema, y sin duda, este novedoso sistema está aquí para quedarse”.